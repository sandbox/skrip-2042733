# Views External Filter

This is a simple module that provides an administration form to select the taxonomies that appear in a views slideshow.

To use this module:

- Enable the module
- Assign permissions for users
- Go to admin/structure/taxonomy/views-external-filter-settings
- Select the vocabulary you want to use
- Select the terms that should show up in your slideshow

- In your view add a contextual filter, using the term field you want to filter on
  - Select "Provide default values" and "PHP code" for the type
  - [Use this gist as the PHP code](https://gist.github.com/skriptble/7631b3c7a4c3484e31cb)
- If you want to filter on nodes instead of terms, select the nodes on the administration page then add a contextual filter for "content: nid"
  - [Use this gist as the PHP code](https://gist.github.com/skriptble/996c4ade9a4373d458db)
